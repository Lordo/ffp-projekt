{-# LANGUAGE PackageImports #-}
import "slp-server" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
