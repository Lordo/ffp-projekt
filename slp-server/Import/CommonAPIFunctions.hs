module Import.CommonAPIFunctions where

import Import
import GameState
import Control.Concurrent
import Control.Monad.Trans.Loop
import System.Random

waitUntilGameChangesIncrease :: Key Game -> Int -> Handler ()
waitUntilGameChangesIncrease gameId initialTimestamp = repeatLoopT $ do
    haveChangesIncreased <- lift $ getSecureItemFromDB gameId (\currentGame -> return (gameChanges currentGame > initialTimestamp)) $ error "Missing game" 
    when haveChangesIncreased exit
    liftIO $ threadDelay (500 * 1000)

waitUntilGameStateIs :: Key Game -> GameState -> Handler ()
waitUntilGameStateIs gameId state = repeatLoopT $ do
    isStateReached <- lift $ getSecureItemFromDB gameId (\currentGame -> return (gameState currentGame == state)) $ error "Missing game" 
    when isStateReached exit
    liftIO $ threadDelay (500 * 1000)

waitUntilEvaluationTimerEnds :: Key Game -> Handler ()
waitUntilEvaluationTimerEnds gameId = repeatLoopT $ do
    hasTimerEnded <- lift $ getSecureItemFromDB gameId (\currentGame -> case gameStartNextRoundOn currentGame of
        Just endTime -> do
            currentTime <- liftIO getCurrentTime
            return $ currentTime > endTime
        Nothing -> return True
        ) $ error "Missing game"
    when hasTimerEnded exit
    liftIO $ threadDelay (1000 * 1000)

startNewRoundInNSeconds :: Key Game -> Int -> Handler (Maybe ThreadId)
startNewRoundInNSeconds gameId seconds = do
    getSecureItemFromDB gameId (\game -> if (gameCurrentRound game >= gameMaxRounds game)
        then do
            runDB $ update gameId [GameState =. Over, GameChanges +=. 1, GameStartNextRoundOn =. Nothing]
            return Nothing
        else do
            runDB $ updateWhere [UserGameId ==. gameId] [UserHasFinishedVoting =. False]
            runDB $ update gameId [GameState =. Starting, GameChanges +=. 1]
            runInnerHandler <- handlerToIO
            tid <- liftIO $ forkIO $ runInnerHandler $ do
                liftIO $ threadDelay (seconds * 1000 * 1000)
                randomLetter <- liftIO $ generateRandomLetter
                runDB $ update gameId [
                    GameCurrentLetter =. singleton randomLetter,
                    GameChanges +=. 1,
                    GameCurrentRound +=. 1,
                    GameState =. InRound,
                    GameStartNextRoundOn =. Nothing
                    ]
            return $ Just tid
        ) $ error "Missing game"

generateRandomLetter :: IO (Char)
generateRandomLetter = do
    randomRIO ('A', 'Z')

forUserInGame :: Key Game -> Key User -> ((Game, User) -> Handler Value) -> Handler Value
forUserInGame gameId userId handleItem = do
    forGame gameId $ \game -> do
        forUser userId $ \user -> if userGameId user == gameId
            then handleItem (game, user)
            else returnJson $ getErrorByCode 301

forGame :: Key Game -> (Game -> Handler Value) -> Handler Value
forGame = forDBItem 101

forUser :: Key User -> (User -> Handler Value) -> Handler Value
forUser = forDBItem 102

forCategory :: Key Category -> (Category -> Handler Value) -> Handler Value
forCategory = forDBItem 103

forAnswer :: Key Answer -> (Answer -> Handler Value) -> Handler Value
forAnswer = forDBItem 104

forVote :: Key Vote -> (Vote -> Handler Value) -> Handler Value
forVote = forDBItem 105

forDBItem :: (SqlBackend ~ PersistEntityBackend a, PersistEntity a) => Int -> Key a -> (a -> Handler Value) -> Handler Value
forDBItem errorId key handleItem = getSecureItemFromDB key handleItem $ do
    returnJson $ getErrorByCode errorId

getSecureItemFromDB :: (SqlBackend ~ PersistEntityBackend a, PersistEntity a) => Key a -> (a -> Handler b) -> Handler b -> Handler b
getSecureItemFromDB key handleItem defaultReturn = do
    maybeItem <- runDB $ get key
    case maybeItem of
        Just item -> handleItem item
        Nothing -> defaultReturn

getErrorByCode :: Int -> Value
getErrorByCode errorId = object
    [ "error" .= getErrorText 0 errorId
    , "errorId" .= errorId
    ]

getErrorText :: Int -> Int -> Text
getErrorText 0 = getErrorTextEnglish
getErrorText _ = getErrorTextEnglish

getErrorTextEnglish :: Int -> Text
getErrorTextEnglish 0 = "Unknown request path"
-- 100 - 199: database related
getErrorTextEnglish 101 = "Game not found"
getErrorTextEnglish 102 = "User not found"
getErrorTextEnglish 103 = "Category not found"
getErrorTextEnglish 104 = "Answer not found"
getErrorTextEnglish 105 = "Vote not found"
-- 200 - 299: lobby related
getErrorTextEnglish 201 = "Games need at least one category"
getErrorTextEnglish 202 = "Game already started"

-- 300 - 399: game related
getErrorTextEnglish 301 = "User does not belong to game"
getErrorTextEnglish 302 = "Game currently not in round"
getErrorTextEnglish 303 = "Supplied category does not belong to game"
getErrorTextEnglish 304 = "Answers for this user already exist"
getErrorTextEnglish 305 = "Every answer has to start with the given letter"
getErrorTextEnglish 306 = "Answers cannot be submitted now"
getErrorTextEnglish 307 = "Evaluation data cannot be requested now"
getErrorTextEnglish 308 = "Votes have already been confirmed"
getErrorTextEnglish 309 = "Other user does not belong to game"
getErrorTextEnglish 310 = "Votes cannot be altered now"
getErrorTextEnglish 311 = "Evaluation cannot be confirmed now"
getErrorTextEnglish 312 = "Results cannot be reviewed yet"

getErrorTextEnglish _ = "Unknown error"