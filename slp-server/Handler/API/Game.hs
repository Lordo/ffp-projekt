module Handler.API.Game where

import Import
import Import.CommonAPIFunctions
import GameState
import Database.Persist.Sql
import System.Timeout.Lifted
import Data.Text                as T
import Data.Time
import Control.Concurrent

--
-- ROUND
--

-- REST handler for requesting data about a game while it is in a round. This handler returnes the information imidiately.
getAPIRoundR :: Int64 -> Int64 -> Handler Value
getAPIRoundR gameIdInt userIdInt = getAPIRoundAfterTimestampR gameIdInt userIdInt (-1)

-- REST handler for requesting data about a game while it is in a round. This handler returnes data only when the information have changed since a given timestamp or after 30 seconds.
getAPIRoundAfterTimestampR :: Int64 -> Int64 -> Int -> Handler Value
getAPIRoundAfterTimestampR gameIdInt userIdInt timestamp = do
    let gameId = (toSqlKey gameIdInt) :: Key Game
    let userId = (toSqlKey userIdInt) :: Key User
    forUserInGame gameId userId $ \ (initialGame, _) -> if gameState initialGame /= Lobby
        then do
            _ <- timeout (30 * 1000 * 1000) $ waitUntilGameChangesIncrease gameId timestamp
            forGame gameId $ \ game -> outputRoundInfo (Entity gameId game) (gameCurrentRound game)
        else returnJson $ getErrorByCode 302

outputRoundInfo :: Entity Game -> Int -> Handler Value
outputRoundInfo (Entity gameId game) roundNum = do
    categories <- runDB $ selectList [CategoryGameId ==. gameId] []
    returnJson $ object
        [ "roundNum" .= roundNum
        , "maxRounds" .= gameMaxRounds game
        , "currentLetter" .= gameCurrentLetter game
        , "hasStarted" .= (gameState game == InRound)
        , "hasEnded" .= (gameState game == Evaluation)
        , "categories" .= (fmap (\(Entity categoryId categoryValue) -> object
            [ "id" .= categoryId
            , "name" .= categoryName categoryValue
            ]) categories)
        , "timestamp" .= gameChanges game
        ]

data JsonSingleAnswer = JsonSingleAnswer
    { categoryId :: Int64
    , answer :: Text
    }

instance FromJSON JsonSingleAnswer where
    parseJSON (Object o)  = JsonSingleAnswer <$> o .: "categoryId" <*> o .: "answer"
    parseJSON _           = error "Invalid json supplied"

data JsonAllAnswers = JsonAllAnswers
    { answers :: [JsonSingleAnswer]
    }

instance FromJSON JsonAllAnswers where
    parseJSON (Object o)  = JsonAllAnswers <$> o .: "answers"
    parseJSON _           = error "Invalid json supplied"

createAnswer :: (Text -> Bool) -> (Key Category -> Handler (Either Int Answer)) -> (Key Game, Game) -> Key User -> JsonSingleAnswer -> Handler (Either Int Answer)
createAnswer checkFunction handleFailedCheck (gameKey, game) userKey jsonAnswer = do
    let categoryKey = (toSqlKey (categoryId jsonAnswer)) :: Key Category
    if checkFunction $ answer jsonAnswer
        then do
            getSecureItemFromDB categoryKey (\ category -> if categoryGameId category == gameKey
                then return $ Right $ Answer (answer jsonAnswer) (gameCurrentRound game) gameKey userKey categoryKey
                else return $ Left 303
                ) $ return $ Left 103
        else handleFailedCheck categoryKey

createVotes :: Key Game -> [Key Answer] -> Handler [[Key Vote]]
createVotes gameKey answerKeys = do
    userEntriesInGame <- runDB $ selectList [UserGameId ==. gameKey] []
    runDB $ forM answerKeys $ \ answerId -> do
        forM userEntriesInGame $ \ (Entity userId _) -> do
            insert $ Vote {
                voteAccept = True,
                voteAnswerId = answerId,
                voteVotingUserId = userId
                }

handleFailedAnswerCheckInRound :: Key Category -> Handler (Either Int Answer)
handleFailedAnswerCheckInRound _ = return $ Left 305

handleFailedAnswerCheckInEvaluation :: Int -> Key Game -> Key User -> Key Category -> Handler (Either Int Answer)
handleFailedAnswerCheckInEvaluation currentRound gameId userId categoryId = do
    return $ Right $ Answer "" currentRound gameId userId categoryId

resolveEither :: [Either a b] -> Either a [b]
resolveEither (x : xs) = case x of
    Left a -> Left a
    Right b -> case resolveEither xs of
        Left a -> Left a
        Right bs -> Right (b : bs)
resolveEither [] = Right []

-- REST handler for sending answers for every category of the current round.
-- Request body shall have the form:
-- {
--   answers : [{
--     categoryId : Int
--     answer : String
--   }]
-- }
postAPISendAnswersR :: Int64 -> Int64 -> Handler Value
postAPISendAnswersR gameIdInt userIdInt = do
    jsonAnswers <- (requireJsonBody :: Handler JsonAllAnswers)
    let gameId = (toSqlKey gameIdInt) :: Key Game
    let userId = (toSqlKey userIdInt) :: Key User
    forUserInGame gameId userId $ \ (game, _) -> if elem (gameState game) [InRound, Evaluation]
        then do
            answersForThisUser <- runDB $ selectFirst [AnswerUserId ==. userId, AnswerRound ==. gameCurrentRound game] []
            case answersForThisUser of
                Just _ -> returnJson $ getErrorByCode 304
                Nothing -> do
                    let failedCheckHandler = if gameState game == InRound
                            then handleFailedAnswerCheckInRound
                            else handleFailedAnswerCheckInEvaluation (gameCurrentRound game) gameId userId
                    let checkFunction = if gameState game == InRound
                            then (\t -> T.head (T.toUpper t) == T.head (gameCurrentLetter game))
                            else (\t -> (t == "") || (T.head (T.toUpper t) == T.head (gameCurrentLetter game)))
                    eitherAnswerList <- mapM (
                        createAnswer
                            checkFunction
                            failedCheckHandler
                            (gameId, game)
                            userId
                        ) $ answers jsonAnswers
                    case resolveEither eitherAnswerList of
                        Left errorCode -> returnJson $ getErrorByCode errorCode
                        Right answerList -> do
                            answerKeys <- runDB $ mapM insert answerList
                            _ <- createVotes gameId answerKeys

                            if gameState game == InRound
                                then runDB $ update gameId [GameChanges +=. 1, GameState =. Evaluation]
                                else runDB $ update gameId [GameChanges +=. 1]
                            returnJson $ object
                                [ "success" .= True
                                ]
        else returnJson $ getErrorByCode 306

--
-- EVALUATION
--

-- REST handler for requesting data about a game while a round is being evaluated. This handler returnes the information imidiately.
getAPIEvaluationR :: Int64 -> Int64 -> Handler Value
getAPIEvaluationR gameIdInt userIdInt = getAPIEvaluationAfterTimestampR gameIdInt userIdInt (-1)

-- REST handler for requesting data about a game while a round is being evaluated. This handler returnes data only when the information have changed since a given timestamp or after 30 seconds.
getAPIEvaluationAfterTimestampR :: Int64 -> Int64 -> Int -> Handler Value
getAPIEvaluationAfterTimestampR gameIdInt userIdInt timestamp = do
    let gameId = (toSqlKey gameIdInt) :: Key Game
    let userId = (toSqlKey userIdInt) :: Key User
    forUserInGame gameId userId $ \ (initialGame, _) -> if elem (gameState initialGame) [Evaluation, Starting]
        then do
            _ <- timeout (30 * 1000 * 1000) $ waitUntilGameChangesIncrease gameId timestamp
            forGame gameId $ \ game -> outputEvaluationInfo (Entity gameId game) userId
        else returnJson $ getErrorByCode 307

outputEvaluationInfo :: Entity Game -> Key User -> Handler Value
outputEvaluationInfo (Entity gameId game) userId = do
    categories <- getCategoriesForGame gameId (gameCurrentRound game) userId
    let initialOutputList = [ "roundNum" .= gameCurrentRound game
                            , "maxRounds" .= gameMaxRounds game
                            , "currentLetter" .= gameCurrentLetter game
                            , "isEvaluationFinished" .= (gameState game /= Evaluation)
                            , "categories" .= categories
                            , "timestamp" .= gameChanges game
                            ]
    outputList <- case gameStartNextRoundOn game of
        Just endingTime -> do
            currentTime <- liftIO getCurrentTime
            let (timeDiff, _) = (properFraction $ diffUTCTime endingTime currentTime) :: (Integer, NominalDiffTime)
            return $ ("endingInNSeconds" .= timeDiff) : initialOutputList
        Nothing -> return initialOutputList
    returnJson $ object outputList
            

getCategoriesForGame :: Key Game -> Int -> Key User -> Handler [Value]
getCategoriesForGame gameId roundNum userId = do
    categories <- runDB $ selectList [CategoryGameId ==. gameId] []
    forM categories (\(Entity categoryId categoryValue) -> do
        answerValues <- getAnswersForCategory roundNum userId categoryId
        return $ object
            [ "id" .= categoryId
            , "name" .= categoryName categoryValue
            , "answers" .= answerValues
            ]
        )

getAnswersForCategory :: Int -> Key User -> Key Category -> Handler [Value]
getAnswersForCategory roundNum userId categoryId = do
    answers <- runDB $ selectList [AnswerCategoryId ==. categoryId, AnswerRound ==. roundNum] []
    forM answers (\ (Entity answerId answer) -> do
        forUser (answerUserId answer) $ \ user -> do
            voteValues <- getVotesForAnswer answerId
            maybeYourVote <- runDB $ selectFirst [VoteAnswerId ==. answerId, VoteVotingUserId ==. userId] []
            case maybeYourVote of
                Just (Entity _ yourVote) -> return $ object
                    [ "userId" .= answerUserId answer
                    , "userName" .= userName user
                    , "answer" .= answerValue answer
                    , "yourVote" .= voteAccept yourVote
                    , "votes" .= voteValues
                    ]
                Nothing -> returnJson $ getErrorByCode 105
            )

getVotesForAnswer :: Key Answer -> Handler [Value]
getVotesForAnswer answerId = do
    votes <- runDB $ selectList [VoteAnswerId ==. answerId] []
    forM votes (\ (Entity _ voteValue) -> do
        return $ Bool $ voteAccept voteValue
        )


data JsonVote = JsonVote
    { accept :: Bool
    }

instance FromJSON JsonVote where
    parseJSON (Object o)  = JsonVote <$> o .: "accept"
    parseJSON _           = error "Invalid json supplied"

-- REST handler for voting on a single answer which is currently evaluated.
-- Request body shall have the form:
-- {
--   accept : Bool
-- }
postAPIVoteR :: Int64 -> Int64 -> Int64 -> Int64 -> Handler Value
postAPIVoteR gameIdInt userIdInt categoryIdInt otherUserIdInt = do
    jsonVote <- (requireJsonBody :: Handler JsonVote)
    let gameId = (toSqlKey gameIdInt) :: Key Game
    let userId = (toSqlKey userIdInt) :: Key User
    let categoryId = (toSqlKey categoryIdInt) :: Key Category
    let otherUserId = (toSqlKey otherUserIdInt) :: Key User
    forUserInGame gameId userId $ \ (game, user) -> if gameState game == Evaluation
        then if userHasFinishedVoting user
            then returnJson $ getErrorByCode 308
            else do
                forCategory categoryId $ \ category -> if categoryGameId category == gameId
                    then do
                        forUser otherUserId $ \ otherUser -> if userGameId otherUser == gameId
                            then do
                                maybeAnswer <- runDB $ selectFirst [
                                    AnswerRound ==. gameCurrentRound game,
                                    AnswerUserId ==. otherUserId,
                                    AnswerCategoryId ==. categoryId
                                    ] []
                                case maybeAnswer of
                                    Just (Entity answerId _) -> do
                                        maybeVote <- runDB $ selectFirst [
                                            VoteAnswerId ==. answerId,
                                            VoteVotingUserId ==. userId
                                            ] []
                                        case maybeVote of
                                            Just (Entity voteId _) -> do
                                                runDB $ update voteId [VoteAccept =. accept jsonVote]
                                                runDB $ update gameId [GameChanges +=. 1]
                                                returnJson $ object
                                                    [ "success" .= Bool True
                                                    ]
                                            Nothing -> returnJson $ getErrorByCode 105
                                    Nothing -> returnJson $ getErrorByCode 104
                            else returnJson $ getErrorByCode 309
                    else returnJson $ getErrorByCode 303
        else returnJson $ getErrorByCode 310

-- REST handler for starting a timer after which current evauluation of a round will end. If the timer already started it is reduced by 10 seconds instead if the user sending this request did not request to send this evaluation previously.
getAPIEndEvaluationR :: Int64 -> Int64 -> Handler Value
getAPIEndEvaluationR gameIdInt userIdInt = do
    let gameId = (toSqlKey gameIdInt) :: Key Game
    let userId = (toSqlKey userIdInt) :: Key User
    forUserInGame gameId userId $ \ (game, user) -> if gameState game == Evaluation
        then if userHasFinishedVoting user
            then returnJson $ getErrorByCode 308
            else case gameStartNextRoundOn game of
                Just time -> do
                    runDB $ update gameId [GameStartNextRoundOn =. Just (addUTCTime (-10) time), GameChanges +=. 1]
                    returnJson $ object
                        [ "success" .= Bool True
                        ]
                Nothing -> do
                    numberOfAnswersInCurrentRound <- runDB $ Database.Persist.Sql.count [AnswerGameId ==. gameId, AnswerRound ==. gameCurrentRound game]
                    numberOfCategories <- runDB $ Database.Persist.Sql.count [CategoryGameId ==. gameId]
                    let integralNumberOfAnswersInCurrentRound = fromIntegral $ numberOfAnswersInCurrentRound
                    let integralNumberOfCategories = fromIntegral $ numberOfCategories
                    let numberOfUsersWhoSentAnswers = (integralNumberOfAnswersInCurrentRound / integralNumberOfCategories) :: Rational
                    let timeToWait = fromRational $ numberOfUsersWhoSentAnswers * 10 - 5
                    time <- liftIO getCurrentTime
                    runDB $ update userId [UserHasFinishedVoting =. True]
                    runDB $ update gameId [GameStartNextRoundOn =. Just (addUTCTime timeToWait time), GameChanges +=. 1]
                    _ <- startEvaluationEndingTimer gameId
                    returnJson $ object
                        [ "success" .= Bool True
                        ]
        else returnJson $ getErrorByCode 311

startEvaluationEndingTimer :: Key Game -> Handler ThreadId
startEvaluationEndingTimer gameId = do
    runInnerHandler <- handlerToIO
    liftIO $ forkIO $ runInnerHandler $ do
        _ <- waitUntilEvaluationTimerEnds gameId
        runDB $ update gameId [GameCurrentLetter =. ""]
        _ <- startNewRoundInNSeconds gameId 5
        return ()

--
-- RESULT
--

-- REST handler for requesting a result of a game which is already over.
getAPIResultsR :: Int64 -> Handler Value
getAPIResultsR gameIdInt = do
    let gameId = (toSqlKey gameIdInt) :: Key Game
    forGame gameId $ \ game -> if gameState game == Over
        then do
            results <- getResultsForGame gameId
            returnJson results
        else returnJson $ getErrorByCode 312

getResultsForGame :: Key Game -> Handler [Value]
getResultsForGame gameId = do
    users <- runDB $ selectList [UserGameId ==. gameId] []

    forM users (\(Entity userId userValue) -> do
        correctAnswers <- getCorrectAnswersForUser userId
        let points = ((Import.length correctAnswers) * 10) :: Int
        return $ object
            [ "name" .= userName userValue
            , "points" .= points
            ]
        )

getCorrectAnswersForUser :: Key User -> Handler [Entity Answer]
getCorrectAnswersForUser userId = do
    answers <- runDB $ selectList [AnswerUserId ==. userId] []
    selectCorrectAnswers answers

selectCorrectAnswers :: [Entity Answer] -> Handler [Entity Answer]
selectCorrectAnswers (answer : xs) = do
    correct <- isAnswerCorrect answer
    otherAnswers <- selectCorrectAnswers xs
    if correct
        then return $ answer : otherAnswers
        else return $ otherAnswers
selectCorrectAnswers [] = do
    return []

isAnswerCorrect :: Entity Answer -> Handler Bool
isAnswerCorrect (Entity answerId _) = do
    correctVotes <- runDB $ Database.Persist.Sql.count [VoteAnswerId ==. answerId, VoteAccept ==. True]
    falseVotes <- runDB $ Database.Persist.Sql.count [VoteAnswerId ==. answerId, VoteAccept ==. False]
    return $ correctVotes > falseVotes

--
-- DEFAULT HANDLER
--

-- Handler for every URL which is not defined by the REST-API itself to return a common error message.
handleAPIFallbackR :: [Text] -> Handler Value
handleAPIFallbackR = return $ returnJson $ getErrorByCode 0