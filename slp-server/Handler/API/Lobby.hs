module Handler.API.Lobby where

import Import
import Import.CommonAPIFunctions
import GameState
import Database.Persist.Sql
import System.Timeout.Lifted


data JsonGame = JsonGame
    { categories :: [Text]
    , rounds :: Int
    }

instance FromJSON JsonGame where
    parseJSON (Object o)  = JsonGame <$> o .: "categories" <*> o .: "rounds"
    parseJSON _           = error "Invalid json supplied"

-- REST handler for creating a game. Id of the game is returned.
-- Request body shall have the form
-- {
--   categories : [String]
--   rounds : Int
-- }
postAPICreateGameR :: Handler Value
postAPICreateGameR = do
    jsonGame <- (requireJsonBody :: Handler JsonGame)

    insertedGameId <- runDB $ insert $ Game {
        gameState = Lobby,
        gameChanges = 0,
        gameMaxRounds = rounds jsonGame,
        gameCurrentRound = 0,
        gameCurrentLetter = pack "",
        gameStartNextRoundOn = Nothing
        }

    case length (categories jsonGame) of
        0 -> returnJson $ getErrorByCode 201
        _ -> do
            _ <- forM (categories jsonGame) $ \ jsonCategoryText -> do runDB $ insert $ Category {
                    categoryName = jsonCategoryText,
                    categoryGameId = insertedGameId
                    }
            returnJson $ object
                [ "gameId" .= insertedGameId
                ]

-- User

data JsonUser = JsonUser { name :: Text }

instance FromJSON JsonUser where
    parseJSON (Object o)  = JsonUser <$> o .: "userName"
    parseJSON _           = error "Invalid json supplied"

-- REST handler for users to join a game. Only a username is needed and the id the user has to use to identifie himself in the future is returned.
-- Request body shall have the form:
-- {
--   categories : [String]
--   rounds : Int
-- }
postAPIJoinLobbyR :: Int64 -> Handler Value
postAPIJoinLobbyR gameIdInt = do
    userData <- (requireJsonBody :: Handler JsonUser)
    let userName = name userData
    let gameId = (toSqlKey gameIdInt) :: Key Game
    forGame gameId $ \ game -> if gameState game /= Lobby
        then returnJson $ getErrorByCode 202
        else do
            insertedUserId <- runDB $ insert $ User {
                userName = userName,
                userGameId = gameId,
                userHasFinishedVoting = False
                }
            runDB $ update gameId [GameChanges +=. 1]
            returnJson $ object
                [ "userId" .= insertedUserId
                ]

-- REST handler for requesting data about a game while it is in the lobby state. This handler returnes the information imidiately.
getAPILobbyR :: Int64 -> Handler Value
getAPILobbyR gameId = getAPILobbyAfterTimestampR gameId $ -1

-- REST handler for requesting data about a game while it is in the lobby state. This handler returnes data only when the information have changed since a given timestamp or after 30 seconds.
getAPILobbyAfterTimestampR :: Int64 -> Int -> Handler Value
getAPILobbyAfterTimestampR gameIdInt timestamp = do
    let gameId = (toSqlKey gameIdInt) :: Key Game
    forGame gameId $ \ game -> do
        _ <- timeout (30 * 1000 * 1000) $ waitUntilGameChangesIncrease gameId timestamp
        outputLobbyInfo $ Entity gameId game

outputLobbyInfo :: Entity Game -> Handler Value
outputLobbyInfo (Entity gameId game) = do
    users <- runDB $ selectList [UserGameId ==. gameId] []
    categories <- runDB $ selectList [CategoryGameId ==. gameId] []
    returnJson $ object
        [ "userNames" .= (fmap (\(Entity _ user) -> userName user) users)
        , "categories" .= (fmap (\(Entity _ category) -> categoryName category) categories)
        , "timestamp" .= gameChanges game
        , "hasStarted" .= (gameState game /= Lobby)
        ]

-- REST handler to receive the username for a given user id.
getAPIUserNameR :: Int64 -> Int64 -> Handler Value
getAPIUserNameR gameIdInt userIdInt = do 
    let gameId = (toSqlKey gameIdInt) :: Key Game
    let userId = (toSqlKey userIdInt) :: Key User
    forUserInGame gameId userId $ \ (_, user) -> returnJson $ object
        [ "userName" .= userName user
        ]

-- REST handler to start a game. Returns whether it was successful.
getAPIStartGameR :: Int64 -> Int64 -> Handler Value
getAPIStartGameR gameIdInt userIdInt = do 
    let gameId = (toSqlKey gameIdInt) :: Key Game
    let userId = (toSqlKey userIdInt) :: Key User
    forUserInGame gameId userId $ \ (game, _) -> do
        if gameState game /= Lobby
            then returnJson $ getErrorByCode 202
            else do
                _ <- startNewRoundInNSeconds gameId 5
                returnJson $ object
                    [ "success" .= True
                    ]
