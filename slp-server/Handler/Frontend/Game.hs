module Handler.Frontend.Game where

import Import
import Text.Julius (RawJS (..))


getGameRoundR :: Int64 -> Int64 -> Handler Html
getGameRoundR gameId userId = do
    defaultLayout $ do
        addScript JSRoutesR
        let (currentRoundSpanId, maxRoundsSpanId, currentLetterSpanId, categoryInputsId, sendAnswersButtonId) = setGameRoundIds
        $(widgetFile "gameround")

setGameRoundIds :: (Text, Text, Text, Text, Text)
setGameRoundIds = ("currentRoundSpan", "maxRoundsSpan", "currentLetterSpan", "categoryInputs", "sendAnswersButton")

getGameEvaluateR :: Int64 -> Int64 -> Handler Html
getGameEvaluateR gameId userId = do
    defaultLayout $ do
        addScript JSRoutesR
        let (currentRoundSpanId, maxRoundsSpanId, currentLetterSpanId, categoryCollectionContainer, endingTimer, remainingTime, sendVotesButtonId) = setGameEvaluateIds
        $(widgetFile "gameevaluate")

setGameEvaluateIds :: (Text, Text, Text, Text, Text, Text, Text)
setGameEvaluateIds = ("currentRoundSpan", "maxRoundsSpan", "currentLetterSpan", "categoryCollectionContainer", "endingTimer", "remainingTime", "sendVotesButtonId")

getGameResultR :: Int64 -> Handler Html
getGameResultR gameId = do
    defaultLayout $ do
        let (winner, gameResults) = setGameResultIds
        $(widgetFile "gameresult")

setGameResultIds :: (Text, Text)
setGameResultIds = ("winner", "gameResults")
