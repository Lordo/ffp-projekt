module Handler.Frontend.Index where

import Import

getIndexR :: Handler Html
getIndexR = do
    defaultLayout $ do
        $(widgetFile "index")
