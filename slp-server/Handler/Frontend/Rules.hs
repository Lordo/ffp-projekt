module Handler.Frontend.Rules where

import Import

getRulesR :: Handler Html
getRulesR = do
    defaultLayout $ do
        $(widgetFile "rules")
