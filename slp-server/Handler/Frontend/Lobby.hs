module Handler.Frontend.Lobby where

import Import
import Text.Julius (RawJS (..))

-- LOBBY CREATION

getCreateLobbyR :: Handler Html
getCreateLobbyR = do
    defaultLayout $ do
        addScript JSRoutesR
        let (standardCategories, ownCategoriesId, addCategoryId, createGameButtonId, roundSliderId) = setCreateLobbyFormIds
        $(widgetFile "createlobby")

setCreateLobbyFormIds :: (Text, Text, Text, Text, Text)
setCreateLobbyFormIds = ("standardCategories", "ownCategoriesId", "addCategoryButton", "createGameButton", "roundSlider")

-- SEE LOBBY

getLobbyR :: Int64 -> Handler Html
getLobbyR gameId = do
    (formWidget, _) <- generateFormPost joinForm
    defaultLayout $ do
        addScript JSRoutesR
        let (categoryCollection, userCollection, userJoinFormId, _, shareLink) = setLobbyIds
        let userWidget = [whamlet|
            <form ##{userJoinFormId} method=post action=#>
                ^{formWidget}
                <button class="btn waves-effect waves-light" type=submit>Mitspielen
            |]
        let userJSLogic = [julius|
            $('##{rawJS userJoinFormId}').submit(function(event) {
                event.preventDefault();

                var userName = $('##{rawJS userJoinFormId} input[type="text"]').first().val();
                if (!userName) {
                    Materialize.toast('Bitte gib einen Namen ein.', 3000, 'rounded teal');
                    return;
                }

                // Make an AJAX request to the server to create a new user
                $.ajax({
                    url: '@{APIJoinLobbyR gameId}',
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        userName: userName
                    }),
                    success: function (data) {
                        if (!data || data.error) {
                            console.log(data);
                            Materialize.toast('Serverseitiger Fehler.', 3000, 'rounded red');
                            return;
                        }
                        window.location.href = jsRoutes.UserLobbyR.get('#{rawJS $ show gameId}', data.userId).url;
                    },
                    error: function () {
                        Materialize.toast('Nutzername könnte nicht gewählt werden.', 3000, 'rounded red');
                    },
                });
            });
            |]
        $(widgetFile "lobby")

getUserLobbyR :: Int64 -> Int64 -> Handler Html
getUserLobbyR gameId userId = do
    defaultLayout $ do
        addScript JSRoutesR
        let (categoryCollection, userCollection, _, startGameId, shareLink) = setLobbyIds
        let userNameHolder = "userNameHolder" :: Text
        let userWidget = [whamlet|
            Du nimmst <span id=#{userNameHolder}></span>am Spiel teil.<br><br>
            <div class="row center-align">
                <a class="btn waves-effect waves-light" id=#{startGameId}>Spiel starten
            |]
        let userJSLogic = [julius|
            gameRedirectUrl = '@{GameRoundR gameId userId}';
            $("##{rawJS startGameId}").click(function() {
                $.ajax({
                    url: '@{APIStartGameR gameId userId}',
                    type: 'GET',
                    contentType: 'application/json',
                    success: function (data) {
                        if (data && data.success) {
                            Materialize.toast('Spiel startet gleich...', 3000, 'rounded teal');
                        } else {
                            Materialize.toast('Fehler beim Spiel starten.', 3000, 'rounded red');
                        }
                    },
                    error: function (data) {
                        console.log("Error creating comment: " + data);
                    }
                });
            })
            $.ajax({
                url: '@{APIUserNameR gameId userId}',
                type: 'GET',
                contentType: 'application/json',
                success: function (data) {
                    if(data && data.userName) {
                        $('##{rawJS userNameHolder}').html('als "' + data.userName + '" ');
                    } else {
                        window.location.href = '@{LobbyR gameId}';
                    }
                },
                error: function (data) {
                    console.log("Error creating comment: " + data);
                }
            })
            |]
        $(widgetFile "lobby")

joinAForm :: AForm Handler Text
joinAForm = areq textField "Spielername" Nothing

joinForm :: Html -> MForm Handler (FormResult Text, Widget)
joinForm = renderTable joinAForm

setLobbyIds :: (Text, Text, Text, Text, Text)
setLobbyIds = ("categoryList", "userList", "joinForm", "startGameButton", "shareLink")