-- @GameState.hs
{-# LANGUAGE TemplateHaskell #-}
module GameState where

import Database.Persist.TH
import Data.Aeson.Types
import Prelude
import Data.Text

data GameState = Lobby | Starting | InRound | Evaluation | Over
    deriving (Show, Read, Eq) -- , Data, Typeable

instance ToJSON GameState  where
    toJSON state = case state of
        Lobby -> "Lobby"
        Starting -> "Starting"
        InRound -> "InRound"
        Evaluation -> "Evaluation"
        Over -> "Over"

createGameState :: Text -> GameState
createGameState "Lobby"     = Lobby
createGameState "Starting"  = Starting
createGameState "InRound"   = InRound
createGameState "Evaluation" = Evaluation
createGameState "Over"      = Over
createGameState _ = error "Invalid GameState"


instance FromJSON GameState where
     parseJSON (String s) = pure $ createGameState s
     parseJSON _          = error "Invalid GameState"

derivePersistField "GameState"