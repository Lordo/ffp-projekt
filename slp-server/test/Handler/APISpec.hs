module Handler.APISpec (spec) where

import TestImport
import Data.Aeson

spec :: Spec
spec = withApp $ do
    describe "lobby" $ do
        it "creates a game" $ do
            --get APICreateGameR
            --statusIs 200

            let categories = ["Test 1", "Test 2"] :: [Text]
                rounds = 2
                body = object
                    [ "categories" .= categories
                    , "rounds" .= Integer rounds
                    ]
                --encoded = encode body

            request $ do
                setMethod "POST"
                setUrl APICreateGameR
                setRequestBody body
                addRequestHeader ("Content-Type", "application/json")
                --addTokenFromCookie
            
            statusIs 200

            --[Entity _id game] <- runDB $ selectList [Game ==. message] []
            --assertEqual "Should have " comment (Comment message Nothing)

    --describe "lobby" $ do
    --    it "400s when the JSON body is invalid" $ do
    --        get HomeR

    --        let body = object [ "foo" .= ("My message" :: Value) ]
    --        request $ do
    --            setMethod "POST"
    --            setUrl CommentR
    --            setRequestBody $ encode body
    --            addRequestHeader ("Content-Type", "application/json")
    --            addTokenFromCookie
    --        statusIs 400

