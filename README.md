Stadt-Land-Programmierframework Online
======================================

Bei diesem Projekt handelt es sich um eine Implementierung des bekannten Spieles Stadt-Land-Fluss als RESTful-Webservice auf der Basis von Yesod (Haskell). Zu Veranschaulichungszwecken ist im Projekt eine Umsetzung einer Webseite enthalten, welche durch AJAX-Anfragen auf die Backend-REST-API zugreift.

Das komplette Projekt ist als Git-Projekt online verf�gbar:

    git clone https://bitbucket.org/Lordo/ffp-projekt.git

Vorschau
--------

Ein Screenshot ist im Projekt enthalten:
![Screenshot](http://i.imgur.com/7V8UsHs.png)


Installation
------------

Das Projekt vewendet den GHC und die Platform Stack zum kompilieren. Zum bauen f�hrt man folgende Komandos im Verzeichnis .../slp-server/ aus:

    stack init
    stack build

Zum Ausf�hren verwendet man dann entweder

    stack exec slp-server

oder falls man selbst Quellcode ver�ndern m�chte und dies live kompiliert haben m�chte verwendet:

    stack exec -- yesod devel